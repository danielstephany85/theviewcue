import {
    FETCHING_DATA,
    SET_GENRES,
    SET_GENRE_MOVIE_LISTS,
    SET_USER,
    LOG_OUT,
    ADD_TO_LIST,
    REMOVE_FROM_LIST,
    SET_ACTIVE_MOVIE
} from "../actions/index";

var initialState = {
    genres: [],
    genreMovieLists: [],
    activeMovieList: [],
    signedIn: false,
    user: undefined,
    token: undefined,
    isFetching: false,
    activeMovieData: undefined
}

export default function rootReducer(state = initialState, action) {
    switch (action.type) {
        case SET_ACTIVE_MOVIE:
            return {
                ...state,
                activeMovieData: action.activeMovieData
            };
        case FETCHING_DATA:
            return {
                ...state,
                isFetching: true,
            };
        case SET_GENRES:
            return {
                ...state,
                isFetching: false,
                genres: action.genres
            };
        case SET_GENRE_MOVIE_LISTS:
            return {
                ...state,
                isFetching: false,
                genreMovieLists: action.genreMovieLists
            };
        case SET_USER:
            return {
                ...state,
                signedIn: true,
                user: action.user,
                token: action.token
            };
        case LOG_OUT:
            return {
                ...state,
                user: undefined,
                signedIn: false
            };
        case ADD_TO_LIST:
            return {
                ...state,
                isFetching: false,
                user: {
                    ...state.user,
                    movieList: action.movieList
                }
            };
        case REMOVE_FROM_LIST:
            let newUserList = [];
            if (state.user.movieList.length){
                newUserList = state.user.movieList.filter(item => action.id !== item._id);
            }
            return {
                ...state,
                isFetching: false,
                user: {
                    ...state.user,
                    movieList: newUserList
                }
            };
        default:
            return state;
    }
}
