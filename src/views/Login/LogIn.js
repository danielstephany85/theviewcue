import React, { Component } from 'react';
import config from "../../config";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setUser } from "../../actions/index";

class LogIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            emailInput: '',
            passwordInput: '',
            emailErr: '',
            passwordErr: '',
        }
    }

    setUser = bindActionCreators(setUser, this.props.dispatch);

    componentDidMount = () => {
        this.emailInput.focus();
    }

    validateInputs = () => {
        let validBool = true;
        let errData = {};
        if (this.state.emailInput === '') {
            errData.emailErr = "email is required";
            validBool = false;
        }
        if (this.state.passwordInput === '') {
            errData.passwordErr = "password is required";
            validBool = false;
        }
        if (!validBool) {
            this.setState({ ...errData });
        }
        return validBool
    }

    handleFetch = (payload) => {
        fetch(`${config.url}/api/authenticate`, {
            body: JSON.stringify(payload), // must match 'Content-Type' header
            headers: { 'content-type': 'application/json' },
            method: 'POST',
            mode: 'cors', // no-cors, cors, *same-origin
        }).then((res) => {
            if (res) {
                return res.json();
            }
        }).then((res) => {
            if(res.status === "success"){
                this.setUser(res.data.user, res.data.token)
                localStorage.setItem('token', res.data.token);
                localStorage.setItem('userId', res.data.user._id);
                this.props.history.push('/');
            } else if (res.status === "error" && res.data.message === "user email was not found"){
                this.setState({ emailErr: "user email was not found"});
            } else if (res.status === "error" && res.data.message === "incorrect password") {
                this.setState({ passwordErr: "incorrect password" });
            }
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({
            emailErr: '',
            passwordErr: '',
        });
        const validBool = this.validateInputs();
        if (validBool) {
            const payload = {
                email: this.state.emailInput,
                password: this.state.passwordInput
            }
            this.handleFetch(payload);
        }
    }

    render = () => {

        return (
            <section>
                <div className="content-card">
                    <div className="content-card__content">
                        <h2>Log In</h2>
                        <form className="main-form" onSubmit={(e) => { this.handleSubmit(e) }}>
                            <div className="main-form__section">
                                <div className={this.state.emailErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>Email</span>
                                        <input type="email" name="email" value={this.state.emailInput} ref={(input)=>{this.emailInput = input}} onChange={(e) => this.setState({ emailInput: e.target.value })} />
                                    </label>
                                    {this.state.emailErr ? <span className="error-message">{this.state.emailErr}</span> : ''}
                                </div>
                                <div className={this.state.passwordErr ? 'main-form__item error' : 'main-form__item'}>
                                    <label>
                                        <span>Password</span>
                                        <input type="password" name="password" value={this.state.passwordInput} onChange={(e) => this.setState({ passwordInput: e.target.value })} />
                                    </label>
                                    {this.state.passwordErr ? <span className="error-message">{this.state.passwordErr}</span> : ''}
                                </div>
                                <div className="main-form__item form-btn-container">
                                    <button className="form-button" type="submit">Log In</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        );
    }
}

export default connect()(LogIn);
