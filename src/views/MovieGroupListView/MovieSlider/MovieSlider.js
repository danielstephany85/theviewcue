import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Poster from '../../../components/Poster';
import MovieSliderIndicators from './MovieSliderIndicators';

class MovieSlider extends Component {
    constructor(props){
        super(props);
        this.state = {
            slidesLength: 0,
            slideIndex: 0,
            sliderSet: false,
            sliderTrackHight: 0,
            sliderTrackWidth: 0,
            sliderTrackPosition: "0%",
        }
        this.previouseItemsPerSlide = undefined; 
        this.itemsPerSlide = undefined;
    }

    componentDidMount =()=>{
        this.setSliderDimentions();
        this.swipeToggle();
    }
    componentWillUnmount = () => {
        window.removeEventListener('resize', this.handleWindowResize, { useCapture: true});
    }

    setSliderDimentions = () => {
        if (window.innerWidth < 700) {
            this.itemsPerSlide = 3;
        } else if (window.innerWidth >= 700 && window.innerWidth < 1200){
            this.itemsPerSlide = 5;
        }
        else {
            this.itemsPerSlide = 7;
        }
        this.previouseItemsPerSlide = this.itemsPerSlide;
        this.setSlideTrack();
        window.addEventListener('resize', this.handleWindowResize, { useCapture: true});
    }

    handleWindowResize = () => {
        if (window.innerWidth < 700) {
            this.itemsPerSlide = 3;
        } else if (window.innerWidth >= 700 && window.innerWidth < 1200) {
            this.itemsPerSlide = 5;
        }
        else {
            this.itemsPerSlide = 7;
        }
        this.setState({
            sliderTrackHight: 0,
            sliderTrackWidth: 0,
        }, () => {
            this.setSlideTrack();
        });
    }

    setSlideTrack = () => {
        const trackwidth = this.slideTrack.clientWidth;
        const slideLength = this.slideItemsToSlides();
        let sliderValues = {
            slidesLength: slideLength,
            sliderTrackHight: this.slideTrack.children[0].clientHeight,
            sliderTrackWidth: trackwidth,
            sliderSet: true
        }
        if (this.previouseItemsPerSlide !== this.itemsPerSlide){
            sliderValues.slideIndex = 0;
            this.previouseItemsPerSlide = this.itemsPerSlide;
        }
        this.setState({
            ...sliderValues
        });
    }

    slideItemsToSlides = () => {
        let slidesNum = this.props.list.length / this.itemsPerSlide;
        if ((slidesNum % 1) !== 0){
            slidesNum = Math.floor(slidesNum) + 1;
        }
        return slidesNum;
    }

    toggleForward = () =>{
        if ((this.state.slidesLength - 1) === this.state.slideIndex )return;
        let slideValue = (this.state.slideIndex === 0)?  (this.state.slideIndex + 100):(this.state.slideIndex * 100 + 100);
        let newIndex = this.state.slideIndex + 1;
        this.setState({
            sliderTrackPosition: `-${slideValue}%`,
            slideIndex: newIndex
        });
    }

    toggleBack = () => {
        if (this.state.slideIndex === 0)return;
        let slideValue = (this.state.slideIndex * 100) - 100;
        let newIndex = this.state.slideIndex - 1;
        this.setState({
            sliderTrackPosition: `-${slideValue}%`,
            slideIndex: newIndex
        });
    }

    swipeToggle = () => {
        const movieSlider = this;
        const slideTrack = this.slideTrack;
        let trackStartPosition;
        let first_Coordinate_X;
        let first_Coordinate_Y;
        let Coordinate_X_Diff;
        let Coordinate_Y_Diff;
        let translateToValue;
        let resetSlidePosition;
        let horizontalScroll = false;
        
        this.slideTrack.addEventListener("touchstart", function(e){
            const trackPosition = parseInt(movieSlider.state.sliderTrackPosition, 10);
            const trackTransformVal = (trackPosition === 0) ? trackPosition : (trackPosition / -100);
            resetSlidePosition = false;
            trackStartPosition = -movieSlider.state.sliderTrackWidth * trackTransformVal;
            first_Coordinate_X = Math.ceil(e.touches[0].clientX);
            first_Coordinate_Y = Math.ceil(e.touches[0].clientY);
            slideTrack.style.transition = 'none';
        });

        this.slideTrack.addEventListener("touchmove", function (e) {
            Coordinate_X_Diff = (first_Coordinate_X * -1) + Math.ceil(e.touches[0].clientX);
            Coordinate_Y_Diff = (first_Coordinate_Y * -1) + Math.ceil(e.touches[0].clientY);
            if (Math.abs(Coordinate_X_Diff) < Math.abs(Coordinate_Y_Diff)){
                horizontalScroll = true;
                return;
            }
            horizontalScroll = false;
            translateToValue = trackStartPosition + Coordinate_X_Diff;
            if (movieSlider.state.slideIndex === 0 && Coordinate_X_Diff > 0){
                slideTrack.style.transform = "translate3d(" + (trackStartPosition + (Coordinate_X_Diff / 3) ) + "px, 0px, 0px)";
                resetSlidePosition = true;
            } else if ((movieSlider.state.slidesLength - 1) === movieSlider.state.slideIndex && Coordinate_X_Diff < 0){
                slideTrack.style.transform = "translate3d(" + (trackStartPosition + (Coordinate_X_Diff / 3)) + "px, 0px, 0px)";
                resetSlidePosition = true;
            }else {
                slideTrack.style.transform = "translate3d(" + translateToValue + "px, 0px, 0px)";
            }
        });

        this.slideTrack.addEventListener("touchend", function () {
            if (horizontalScroll) resetSlidePosition = true;
            slideTrack.style.transition = "transform 0.8s ease";
            if (Coordinate_X_Diff > 0){
                if (resetSlidePosition || Coordinate_X_Diff <= 50){
                    slideTrack.style.transform = "translate3d(" + movieSlider.state.sliderTrackPosition + ", 0px, 0px)";
                } else if (Coordinate_X_Diff > 50){
                    setTimeout(() => { movieSlider.toggleBack(); }, 10);
                }
            } else if (Coordinate_X_Diff < 0){
                if (resetSlidePosition || Coordinate_X_Diff >= -50) {
                    slideTrack.style.transform = "translate3d(" + movieSlider.state.sliderTrackPosition + ", 0px, 0px)";
                } else if (Coordinate_X_Diff < -50){
                    setTimeout(() => { movieSlider.toggleForward(); }, 10);
                }
            }
            trackStartPosition = undefined;
            first_Coordinate_X = undefined;
            first_Coordinate_Y = undefined;
            Coordinate_X_Diff = undefined;
            Coordinate_Y_Diff = undefined;
            translateToValue = undefined;
            resetSlidePosition = undefined;
            horizontalScroll = false;
        });
    }

    render = () => {
        const slides = this.props.list.map((item, i)=>{
                return(
                    <div key={i} className="movie-slider__item" onClick={() => { this.props.toggleDrawer(item.id) }}>
                        <Poster movieData={item} />
                    </div>
                );
        });

        let sliderLoading = this.state.sliderSet ? '' : 'loading';
        let sliderTrackStyles = {
            height: this.state.sliderTrackHight,
            width: this.state.sliderTrackWidth? this.state.sliderTrackWidth:'auto',
            transform: `translate3d(${this.state.sliderTrackPosition}, 0px, 0px)`
        }

        return(
            <div className={`movie-slider ${sliderLoading}`}>
                <button type="button" className={`movie-slider__back ${(this.state.slideIndex === 0)? 'off':""}`} onClick={this.toggleBack}></button>
                <button type="button" className={`movie-slider__forward ${((this.state.slidesLength - 1) === this.state.slideIndex) ? 'off' : ""}`} onClick={this.toggleForward}></button>
                <MovieSliderIndicators activeSlide={this.state.slideIndex} slidesLength={this.state.slidesLength}/>
                <div className="movie-slider__content">
                    <div className="movie-slider__track" style={sliderTrackStyles} ref={(div)=>{this.slideTrack = div}}>
                        {slides}
                    </div>
                </div>
            </div>
        );
    }
}

MovieSlider.propTypes = {
    list: PropTypes.array.isRequired,
    toggleDrawer: PropTypes.func.isRequired
}

export default MovieSlider;