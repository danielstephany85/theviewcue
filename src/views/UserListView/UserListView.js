import React, {Component} from 'react';
import PropTypes from "prop-types";
import Pagination from './Pagination';
import Poster from '../../components/Poster';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

class UserListView extends Component {
    constructor(props){
        super(props);
        this.state = {
            movieList: undefined,
            page: undefined
        }
        this.unmount = false;
    }
    componentWillMount = () => {
        this.searchParams = this.getSearchParams();
        try{
            if (this.this.searchParams.page !== undefined) {
                this.setState({ page: this.searchParams.page });
            } else {
                throw new Error('Invalid page value');
            }
        }catch(e){
            this.setState({ page: 1 });
        }
    }

    componentDidUpdate = () => {
        window.scrollTo(0, 0);
        this.searchParams = this.getSearchParams();
        try{
            if (this.state.page !== this.searchParams.page && this.searchParams.page !== undefined){
               
                this.setState({page: this.searchParams.page});
            }else{
                throw new Error('Invalid page value');
            }
        }catch(e){
            if (!this.searchParams.page) {
                this.searchParams.page = 1;
            }
        }
    }

    getSearchParams = () => {
        if (!this.props.location.search) return {};
        let searchParams = {};
        var queryParams = this.props.location.search;
        if (this.props.location.search[0] === "?") queryParams = queryParams.slice(1);
        queryParams = queryParams.split("?");
        queryParams.forEach((query) => {
            let qp = query.split("=");
            qp[1] = qp[1].replace('%20',' ');
            searchParams[qp[0]] = qp[1];
        });
        return searchParams;
    }

    render = () => {
        let movieListItems;
        let totalPages;
        try{
            totalPages = Math.ceil(this.props.user.movieList.length / 20);
            //setting movieListItems to only be served 20 items at a time with page
            movieListItems = this.props.user.movieList.filter((item, i) => (i >= (this.state.page * 20 - 20) && i < (this.state.page * 20)));
            // console.dir(this.props.user.movieList);
            movieListItems = movieListItems.map((data, i) => {
                return <NavLink to={`/movie-card-view/${data.movieId}`} key={i} className="movie-list__item"><Poster listView={true} movieData={data} /></NavLink>
            });
        }catch(e){
            movieListItems = <span>Woops! this is an empty list</span>;
        }

        return(
            <div className="movie-list">
                <h3>Your List</h3>
                <div className="movie-list__row">
                    {movieListItems ? movieListItems:undefined}
                </div>
                <div className="pagination-container">
                    <h3>{this.state.genre}</h3>
                    {(totalPages > 1) ? <Pagination page={parseInt(this.state.page, 10)} total_pages={totalPages} /> : undefined}
                </div>
            </div>
        );
    }
}

UserListView.propTypes = {
    user: PropTypes.object
}

const mapStateToProps = state => ({user: state.user});

export default connect(mapStateToProps)(UserListView);