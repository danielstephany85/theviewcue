import React, {Component} from 'react';
import MovieInfoCard from '../../components/MovieInfoCard';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { setActiveMovieData } from "../../actions/index";
import config from '../../config';

class MovieCardView extends Component {

    constructor(props){
        super(props);
        this.lastId = '';
    }

    setActiveMovieData = bindActionCreators(setActiveMovieData, this.props.dispatch);

    componentDidMount = () => {
        this.fetchMovie(this.props.match.params.id);
    }

    componentDidUpdate = () => {
        if (this.props.match.params.id !== this.lastId){
            this.fetchMovie(this.props.match.params.id);
        }
    }

    fetchMovie = (id) => {
        this.lastId = this.props.match.params.id;
        fetch(`https://api.themoviedb.org/3/movie/${id}?api_key=${config.apiKey}&language=en-US`)
            .then((res) => {
                let jsonRes = res.json();
                return jsonRes;
            })
            .then((json) => {
                this.setActiveMovieData(json);
            });
    }

    render = () =>{
        return (
            <div className="movie-card-view">
                <button onClick={this.props.history.goBack} className="back-btn"><i className="fas fa-arrow-left"></i></button>
                {this.props.activeMovieData ? <MovieInfoCard />:"loading..."}
            </div>
        );
    }
}

const mapStateToProps = state => ({activeMovieData: state.activeMovieData });

export default connect(mapStateToProps)(MovieCardView);