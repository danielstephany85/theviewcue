import React, {Component} from 'react';
import PropTypes from "prop-types";
import Pagination from './Pagination';
import Poster from '../../components/Poster';
import { NavLink } from 'react-router-dom';
import config from '../../config';


class SearchListView extends Component {
    constructor(props){
        super(props);
        this.state = {
            list: undefined,
            query: undefined
        }
        this.unmount = false;
    }
    componentWillMount = () => {
        this.searchParams = this.getSearchParams();
    }

    componentDidMount = () => {
        this.getMovieList(this.props.match.params.query, this.searchParams.page);
    }

    componentDidUpdate = () => {
        window.scrollTo(0, 0);
        this.searchParams = this.getSearchParams();
        if (!this.searchParams.page){
            this.searchParams.page = 1;
        }
        if (this.state.query !== undefined){
            if (this.state.query !== this.props.match.params.query || parseInt(this.searchParams.page, 10) !== parseInt(this.state.page, 10)) {
                this.getMovieList(this.props.match.params.query, this.searchParams.page);
            }  
        }
    }

    getSearchParams = () => {
        if (!this.props.location.search) return {};
        let searchParams = {};
        var queryParams = this.props.location.search;
        if (this.props.location.search[0] === "?") queryParams = queryParams.slice(1);
        queryParams = queryParams.split("?");
        queryParams.forEach((query) => {
            let qp = query.split("=");
            qp[1] = qp[1].replace('%20',' ');
            searchParams[qp[0]] = qp[1];
        });
        return searchParams;
    }

    getMovieList = (query, page=1) => {
        fetch(`https://api.themoviedb.org/3/search/movie?api_key=${config.apiKey}&language=en-US&query=${query}&page=${page}&include_adult=false`)
            .then((res) => {
                let jsonRes = res.json();
                return jsonRes;
            })
            .then((json) => {
                this.setState({ 
                    page: parseInt(page, 10),
                    total_pages: json.total_pages,
                    list: json.results,
                    query: query,
                 });
            });
    }


    render = () => {
        let movies = undefined;
        if(this.state.list){
            let filteredList = this.state.list.filter((data) => data.backdrop_path);
            movies = filteredList.map((data, i)=>{
                return <NavLink to={`/movie-card-view/${data.id}`} key={i} className="movie-list__item"><Poster listView={true} movieData={data} /></NavLink>
            });
        }

        return(
            <div className="movie-list">
                <div className="movie-list__row">
                    {movies? movies:undefined}
                </div>
                <div className="pagination-container">
                    <h3>{this.state.genre}</h3>
                    {(this.state.total_pages > 1) ? <Pagination page={this.state.page} total_pages={this.state.total_pages} query={this.props.match.params.query} /> : undefined}
                </div>
            </div>
        );
    }
}

SearchListView.propTypes = {
    ApiKey: PropTypes.string.isRequired,
}

export default SearchListView;