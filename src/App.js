import React, { Component } from 'react';
import { 
  BrowserRouter,
  Switch,
  Route,
  // Redirect
 } from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './components/Header';
import MovieGroupListView from './views/MovieGroupListView/MovieGroupListView';
import LogIn from './views/Login/LogIn';
import SignUp from './views/SignUp/SignUp';
import MovieListView from './views/MovieListView/MovieListView';
import SearchListView from './views/SearchListView/SearchListView';
import UserListView from './views/UserListView/UserListView';
import MovieCardView from './views/MovieCardView/MovieCardView';
import Footer from './components/Footer';
import * as Actions from "./actions/index";
import config from "./config";
 
class App extends Component {

  history = createBrowserHistory();
  getGenre = bindActionCreators(Actions.getGenre, this.props.dispatch);
  getGenreMovieList = bindActionCreators(Actions.getGenreMovieList, this.props.dispatch);
  setUser = bindActionCreators(Actions.setUser, this.props.dispatch);
  logOut = bindActionCreators(Actions.logOut, this.props.dispatch);

  componentWillMount = () => {
    const userId = localStorage.getItem("userId");
    const token = localStorage.getItem("token");
    if (token && userId) {
      this.signIn(userId, token);
    }
  }

  componentDidMount = () => { 
    this.getGenre(config.apiKey)
      .then((genres)=>{
        this.getGenreMovieList(config.apiKey, genres);
      });
  }

  signIn = (userId, token) => {
    fetch(`${config.url}/api/user/${userId}?token=${token}`)
    .then((res) => {
      if (res) {
        return res.json();
      }
    }).then((res) => {
      if(res.status === "success"){
        this.setUser(res.data.user, token);
        localStorage.setItem('token', res.data.token);
        localStorage.setItem('userId', res.data.user._id);
      }
    })
  }


  render() {
    return (
      <BrowserRouter>
        <div className="site-constraint">
          <Route path="/" render={(props) => <Header {...props} signedIn={this.props.signedIn} logOut={this.logOut}/>} />
          <div className="main-content">
            <Switch>
              <Route exact path="/" render={(props)=><MovieGroupListView {...props}/>}/>
              <Route exact path="/log-in" render={(props) => <LogIn {...props} />} />
              <Route exact path="/sign-up" render={(props) => <SignUp {...props}/>} />
              <Route exact path="/movie-list-view/:id" render={(props) => <MovieListView {...props}/>} />
              <Route exact path="/search-list-view/:query" render={(props) => <SearchListView {...props}/>} />
              <Route exact path="/user-list-view" render={(props) => <UserListView {...props} />} />
              <Route exact path="/movie-card-view/:id" render={(props) => <MovieCardView {...props}/>} />
            </Switch>
          </div>
          <Footer/>
        </div>
      </BrowserRouter>
    );
  }
}

const mapStateToProps = state => (
  {
    genres: state.genres,
    genreMovieLists: state.genreMovieLists,
    activeMovieList: state.activeMovieList,
    signedIn: state.signedIn,
    user: state.user,
    token: state.token,
    isFetching: state.isFetching
  }
);

export default connect(mapStateToProps)(App);