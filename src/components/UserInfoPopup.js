import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {NavLink} from 'react-router-dom';

class UserInfoPopup extends Component {

    state = { Active: false};

    componentDidMount = () => {
        document.addEventListener("click", this.closeDrawer, { useCapture: true });
        document.addEventListener("touchend", this.closeDrawer, { useCapture: true });
    }

    componentWillUnmount = () => {
        document.removeEventListener("click", this.closeDrawer, { useCapture: true });
        document.removeEventListener("touchend", this.closeDrawer, { useCapture: true });
    }

    closeDrawer = (e) => {
        if(!this.state.Active) return;
        let clickInDrawer = false;
        checkParent(e.target);
        function checkParent(target) {
            if (target.classList) {
                target.classList.forEach((item) => {
                    if (item === "user-info-popup__drawer") clickInDrawer = true;
                });
            }
            if (target.parentNode) {
                return checkParent(target.parentNode);
            }
        }
        if (!clickInDrawer && this.state.Active) {
            this.setState({ Active: !this.state.Active });
        }
    }

    toggleDrawer = (e) => {
        e.nativeEvent.stopImmediatePropagation();
        this.setState({Active: !this.state.Active});
    }

    render = () => {
        return(
            <div href="" className={`icon-item user-info-popup ${this.state.Active? 'active':""}`} onClick={(e)=>{this.toggleDrawer(e)}}>
                <i className="fas fa-user"></i>
                <div className="user-info-popup__drawer">
                    <NavLink to="/user-list-view">your list</NavLink>
                    <a onClick={this.props.logOut}>sign out</a>
                </div>    
            </div>
        );
    }
}

UserInfoPopup.propTypes = {
    history: PropTypes.object.isRequired,
    logOut: PropTypes.func.isRequired
}

export default UserInfoPopup;