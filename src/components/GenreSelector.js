import React, {Component} from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

class GenreSelector extends Component {

    constructor(props){
        super(props);
        this.drawerOpen = false;
        this.resizing = false;
    }

    componentDidMount = () => {
        this.setDrawer();
        document.addEventListener("click", this.closeDrawer, { useCapture: true });
        document.addEventListener("touchend", this.closeDrawer, { useCapture: true });
    }

    componentWillUnmount = () => {
        document.removeEventListener("click", this.closeDrawer, { useCapture: true });
        document.removeEventListener("touchend", this.closeDrawer, { useCapture: true });
    }

    //add resize event

    setDrawer = () => {
        this.drawerHeight = this.drawerContent.clientHeight;
    }

    toggleDrawer = (e) => {
        e.nativeEvent.stopImmediatePropagation();
        if(!this.drawerOpen){
            this.drawer.style.height = this.drawerHeight+'px';
            this.drawerOpen = true;
        }else {
            this.drawer.style.height = 0;
            this.drawerOpen = false;
        }

    }

    closeDrawer = (e) => {
        if (!this.drawerOpen) return;
        let clickInDrawer = false;
        checkParent(e.target);
        function checkParent(target){
            if (target.classList){
                target.classList.forEach((item) => {
                    // if (item === "genre-selector__drawer" || item === "main-btn") clickInDrawer = true;
                    if (item === "genre-button") clickInDrawer = true;
                });
            }   
            if (target.parentNode){
                return checkParent(target.parentNode);
            }
        }
        if(!clickInDrawer && this.drawerOpen){
            this.drawer.style.height = 0;
            this.drawerOpen = false;
        }
    }

    render = (props) => {

        const genreLinks = this.props.genres.map((item, i) => {
            return <NavLink className="genre-link" to={`/movie-list-view/${item.id}?genre=${item.name}?page=1`} key={i} ><span>{item.name}</span></NavLink>
        });

        return(
            <div className='genre-selector'>
                <button className="main-btn genre-button" onClick={(e)=>{this.toggleDrawer(e)}}>Genres</button>
                <div className="genre-selector__drawer" ref={(div) => { this.drawer = div }}>
                    <div className="genre-selector__drawer-content" ref={(div) => { this.drawerContent = div }}>
                        {genreLinks}
                    </div>
                </div>
            </div>
        );
    }
}

GenreSelector.propTypes = {
    genres: PropTypes.array.isRequired,
}

export default GenreSelector;