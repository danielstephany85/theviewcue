# This is the front end of theviewcue built with create react app.

### Quick Start
to view this project: 

* first open up a terminal and navigate to the root of the project which contains the package.json
* next install the dependancies using Yarn with or npm. 
* once all dependancies are install you can then run: yarn start  or if using npm run: npm run start.
* the dev server should start up and you should be able to open a web browser and navigate to localhost:3000 to view the site.

##### (if the site is run this way it will not be hooked up to the data base so some functionality will be unavailable)